data = open("input.txt").read().splitlines()

tempNumber = 10000;

result = 0;
count = 0;

for x in data:
	tempCount = 0;
	number = 0;

	while tempCount < 3:
		offset = count + tempCount;
		number += int(data[offset]);
		tempCount += 1;

	if number > tempNumber:
		result += 1;

	if count >= (len(data) - 3):
		break;

	tempNumber = number;
	count += 1;

print(result);

#print(result);