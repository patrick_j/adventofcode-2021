<?php 

$file = file_get_contents('./input.txt', true);

$input = explode(PHP_EOL, $file);

$y = 0;
$depth = 0;
$aim = 0;

foreach($input as $i) {
	$x = explode(" ", $i);
	switch($x[0]) {
		case "forward":
			$y += $x[1];
			$depth += ($aim*$x[1]);
			break;
		case "down":
			$aim += $x[1];
			break;
		case "up":
			$aim -= $x[1];
			break;

	}
}

$result = $y * $depth;

print_r($result);
die();
 
?>